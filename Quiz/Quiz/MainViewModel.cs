﻿using System;
using System.Collections.Generic;
using Android.Arch.Core.Util;
using Android.Arch.Lifecycle;
using Java.Lang;

namespace Quiz
{
    public class MainViewModel : ViewModel
    {
        private readonly List<(string, bool)> questions =
            new List<(string, bool)> {
            ("Das Videospiel Donkey Kong sollte ursprünglich Popeye als Hauptfigur haben.", true),
            ("Die Farbe Orange wurde nach der Frucht benannt.", true),
            ("In der griechischen Mythologie ist Hera die Göttin der Ernte.", false),
            ("Liechtenstein hat keinen eigenen Flughafen.", true),
            ("Die meisten Subarus werden in China hergestellt.", false)};

        public int AnsweredQuestions => CorrectAnswers + FalseAnswers
                                                       + SkippedQuestions;
        public int CorrectAnswers { get; set; } = 0;
        public int FalseAnswers { get; set; } = 0;
        public int SkippedQuestions { get; set; } = 0;

        public LiveData Question = new MutableLiveData();        

        private MutableLiveData index = new MutableLiveData();
        public LiveData Index
        {
            get => index;            
        }

        private MutableLiveData answer = new MutableLiveData();
        public LiveData Answer
        {
            get => answer;            
        }

        public MainViewModel()
        {
            index.SetValue(0);
            answer.SetValue("");
            Question = Transformations.Map(Index, new IndexTransformation(questions));
        }

        private class IndexTransformation : Java.Lang.Object, IFunction
        {
            private List<(string, bool)> questions;

            public IndexTransformation(List<(string, bool)> questions)
            {
                this.questions = questions;
            }

            public Java.Lang.Object Apply(Java.Lang.Object p0)
            {
                return questions[(int)p0].Item1;
            }
        }

        public void SkipQuestion()
        {
            SkippedQuestions++;
            IncreaseIndex();
        }

        public void IncreaseIndex()
        {
            var newIndex = ((int)index.Value + 1) % questions.Count;
            index.SetValue(newIndex);
            answer.SetValue("");
        }

        public void EvaluateAnswer(bool givenAnswer)
        {
            if (givenAnswer == questions[(int)index.Value].Item2)
            {
                answer.SetValue("Richtig!");
                CorrectAnswers++;
            } else
            {
                answer.SetValue("Falsch!");
                FalseAnswers++;
            }
            IncreaseIndex();
        }

    }
}
