﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Arch.Lifecycle;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Quiz
{
    public class StatisticsFragment : Android.Support.V4.App.Fragment
    {
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.fragment_statistics, container, false);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            var viewModel = ViewModelProviders.Of(Activity).Get(Java.Lang.Class.FromType(typeof(MainViewModel))) as MainViewModel;
            Activity.FindViewById<TextView>(Resource.Id.questionsTextView).Text = viewModel.AnsweredQuestions.ToString();
            Activity.FindViewById<TextView>(Resource.Id.correctQuestionsTextView).Text = viewModel.CorrectAnswers.ToString();
            Activity.FindViewById<TextView>(Resource.Id.wrongQuestionsTextView).Text = viewModel.FalseAnswers.ToString();
            Activity.FindViewById<TextView>(Resource.Id.skippedQuestionsTextView).Text = viewModel.SkippedQuestions.ToString();            
        }
    }
}
