﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Arch.Lifecycle;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Quiz
{
    public class MainFragment : Android.Support.V4.App.Fragment
    {
        private MainViewModel viewModel;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            HasOptionsMenu = true;            
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            return inflater.Inflate(Resource.Layout.fragment_main, container, false);
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.main,menu);
        }

        public override void OnActivityCreated(Bundle savedInstanceState)
        {
            base.OnActivityCreated(savedInstanceState);
            viewModel = ViewModelProviders.Of(Activity).Get(Java.Lang.Class.FromType(typeof(MainViewModel))) as MainViewModel;
            var questionTextView = Activity.FindViewById<TextView>(Resource.Id.questionTextView);
            viewModel.Question.Observe(this, new ChangeObserver(questionTextView));
            viewModel.Answer.Observe(this, new ToastObserver(Activity));            
            
            var trueButton = Activity.FindViewById<Button>(Resource.Id.trueButton);
            trueButton.Click += TrueButton_Click;

            var falseButton = Activity.FindViewById<Button>(Resource.Id.falseButton);
            falseButton.Click += FalseButton_Click;

            var skipButton = Activity.FindViewById<Button>(Resource.Id.skipButton);
            skipButton.Click += SkipButton_Click;
        }

        private void SkipButton_Click(object sender, EventArgs e)
        {
            viewModel.SkipQuestion();
        }

        private void FalseButton_Click(object sender, EventArgs e)
        {
            viewModel.EvaluateAnswer(false);            
        }

        private void TrueButton_Click(object sender, EventArgs e)
        {
            viewModel.EvaluateAnswer(true);
        }


        private class ToastObserver : Java.Lang.Object, IObserver
        {
            private Context context;

            public ToastObserver(Context context)
            {
                this.context = context;
            }

            public void OnChanged(Java.Lang.Object p0)
            {
                var text = p0.ToString();
                if (!text.Trim().Equals("")) { 
                Toast.MakeText(context, p0.ToString(), ToastLength.Short).Show();
                }
            }
        }

        private class ChangeObserver : Java.Lang.Object, IObserver
        {
            private TextView textView;

            public ChangeObserver(TextView textView)
            {
                this.textView = textView;
            }

            public void OnChanged(Java.Lang.Object p0)
            {
                textView.Text = p0.ToString();
            }
        }
    }
}
